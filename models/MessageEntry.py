class MessageEntry:
    def __init__(self, message: str, message_from: str, message_type: str, is_own: bool):
        self.message = message
        self.message_from = message_from
        self.message_type = message_type
        self.is_own = is_own
