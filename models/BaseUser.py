from crypto.RSAKeyManager import RSAKeyManager


class BaseUser:
    def __init__(self, username: str, url: str, port: int, public_key: bytes):
        self.username = username
        self.url = url
        self.port = port
        self.public_key = RSAKeyManager.get_public_key_from_bytes(public_key)
