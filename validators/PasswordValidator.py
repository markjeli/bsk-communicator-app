import re


class PasswordValidator:
    @staticmethod
    def validate(password: str):
        if password == '':
            return 'Password cannot be empty', False
        if len(password) < 8:
            return 'Password must have at least 8 characters', False
        if not any(char.isupper() for char in password):
            return 'Password must contain at least one uppercase letter', False
        if not re.search("[\W_]", password):
            return "Password must contain at least one special character", False

        return '', True
