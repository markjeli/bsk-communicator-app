import re


class LoginValidator:
    @staticmethod
    def validate(login):
        if not login:
            return 'Login cannot be empty', False
        if len(login) < 5:
            return 'Login must have at least 5 characters', False
        if re.search("[\W_]", login):
            return 'Login cannot contain special characters', False

        return '', True
