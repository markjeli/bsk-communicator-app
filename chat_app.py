import io
import math
import os
import pickle
import random
import socket
import threading
import time
import tkinter as tk
from tkinter.filedialog import askopenfilename
from typing import Dict, List

import customtkinter as ctk

import widgets
from const import (
    CONNECT,
    DISCONNECT,
    DATA_CHUNK, USER_DIR_PREFIX,
)
from crypto.AESEncryptionManager import AESEncryptionManager
from crypto.LocalKeyManager import LocalKeyManager
from crypto.RSAKeyManager import RSAKeyManager
from crypto.SessionKeyGenerator import SessionKeyGenerator
from models.BaseUser import BaseUser
from models.MessageEntry import MessageEntry
from widgets.Color import Color
from widgets.ScrollableMessagesList import ScrollableMessagesList

ctk.set_appearance_mode("dark")


class Peer:
    def __init__(self, peer_socket: socket.socket, username: str | None, session_key: bytes | None = None):
        self.socket: socket.socket | None = peer_socket
        self.username: str = username
        self.session_key: bytes | None = session_key
        self.messages: List[MessageEntry] = list()
        self.messages_list: ScrollableMessagesList | None = None
        self.entry: ctk.CTkEntry | None = None
        self.upload_label: ctk.CTkLabel | None = None
        self.progress_frame: ctk.CTkFrame | None = None

    def set_socket(self, peer_socket: socket.socket):
        self.socket = peer_socket

    def set_messages_list(self, messages_list: tk.Listbox):
        self.messages_list = messages_list

    def set_entry(self, entry: ctk.CTkEntry):
        self.entry = entry

    def set_upload_label(self, upload_label: ctk.CTkLabel):
        self.upload_label = upload_label

    def set_progress_frame(self, progress_frame: ctk.CTkFrame):
        self.progress_frame = progress_frame


def find_available_port() -> int:
    while True:
        port = random.randint(49152, 65535)
        if is_port_available(port):
            return port


def is_port_available(port) -> bool:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.settimeout(5)
        return sock.connect_ex(('127.0.0.1', port)) != 0


class ChatApp:
    def __init__(self):
        self.server_socket: socket.socket | None = None
        self.receive_socket: socket.socket | None = None
        self.peers: Dict[str: Peer] = {}
        self.users: List[BaseUser] = list()
        self.encryption_type: str = "ECB"

        self.login_window = widgets.LoginWindow()
        self.login_window.mainloop()
        self.username = self.login_window.entry_login.get()
        self.login_window.destroy()
        self.user_directory = f"{USER_DIR_PREFIX}{self.username}/"

        self.stop_event = threading.Event()

        self.server_url = '127.0.0.1'
        self.server_port = 12345

        self.client_url = '127.0.0.1'
        self.client_port = find_available_port()

        self.window = widgets.App()
        self.window.update_user_info(f'Username: {self.username} ({self.client_url}:{self.client_port})')
        self.window.users_list.command = self.connect_to_user
        self.window.set_messages_tab_command(self.refresh_messages_tab)

        self.window.connection_option.configure(command=self.manage_connection)
        self.window.encryption_option.configure(command=self.select_encryption)

        threading.Thread(target=self.listen_for_messages).start()

        self.window.mainloop()
        self.stop_event.set()
        self.server_socket.shutdown(socket.SHUT_RDWR)
        self.receive_socket.shutdown(socket.SHUT_RDWR)
        exit(1)

    def select_encryption(self, choice):
        self.encryption_type = choice

    def manage_connection(self, choice):
        if choice == CONNECT:
            self.window.connection_label.configure(text="Connected", fg_color="green")
            threading.Thread(target=self.connect_to_server).start()
        elif choice == DISCONNECT:
            if self.server_socket:
                self.window.connection_label.configure(text="Not Connected", fg_color=("red", "black"))
                self.server_socket.shutdown(socket.SHUT_RDWR)

    def connect_to_server(self):
        if self.stop_event.is_set():
            return
        else:
            self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = self.server_socket.connect_ex((self.server_url, self.server_port))

            if result != 0:
                print(f'Failed to connect to the server! Retrying...')
                time.sleep(1)
                return self.connect_to_server()

            print(f'Successfully connected to the server at {self.server_url}:{self.server_port}')
            self.login()
            threading.Thread(target=self.receive_from_server).start()

    def listen_for_messages(self):
        self.receive_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.receive_socket.bind((self.client_url, self.client_port))
        self.receive_socket.listen(1)
        print(f'Listening for connections on {self.receive_socket.getsockname()}')

        while True and not self.stop_event.is_set():
            try:
                peer_socket, address = self.receive_socket.accept()
                peer = Peer(peer_socket, None)
                print(f'New connection from: {address}')
                threading.Thread(target=self.handle_messages, args=(peer,)).start()
            except Exception:
                print('Failed to accept new connection!')
                pass

    def login(self):
        rsa_key_manager = RSAKeyManager(keys_directory=f'user-{self.username}')
        public_key = rsa_key_manager.load_public_key()
        public_key_bytes = rsa_key_manager.get_bytes_from_public_key(public_key)

        self.server_socket.send(pickle.dumps({
            'type': 'login',
            'username': self.username,
            'url': self.client_url,
            'port': self.client_port,
            'public_key': public_key_bytes
        }))

    def receive_from_server(self):
        while True and not self.stop_event.is_set():
            data: bytes = self.server_socket.recv(4096)
            if not data and self.stop_event.is_set():
                print('Lost connection to the server! Retrying...')
                self.connect_to_server()
                break

            data: dict = pickle.loads(data)

            if 'users_list' in data.get('type'):
                self.refresh_users_list(data.get('users'))

    def upload_file(self, peer: Peer):
        filename = askopenfilename(filetypes=[('Any Files', '*.*')])
        file = open(filename, 'rb')

        if file is None:
            return

        threading.Thread(target=self.progress_bar_advance, args=(file, peer,)).start()

    def progress_bar_advance(self, file: io, peer: Peer):
        start = time.perf_counter_ns()
        file_size = os.path.getsize(file.name)
        step = 1 / (file_size / DATA_CHUNK)

        progress_bar = ctk.CTkProgressBar(
            master=peer.progress_frame,
            orientation=tk.HORIZONTAL,
            width=300,
            mode='determinate',
            determinate_speed=step
        )
        progress_bar.set(0)
        progress_bar.pack(fill=ctk.BOTH)
        file_name = file.name.split("/")[-1]
        message_entry = MessageEntry(f'Uploading file "{file_name}"', 'ME', 'file-exchange', is_own=True)
        peer.messages_list.add_item(message_entry)
        peer.socket.send(pickle.dumps({
            'type': 'file',
            'size': file_size,
            'name': file_name,
            "encryption": self.encryption_type
        }))
        encryption_key, iv = peer.session_key[:16], peer.session_key[16:32]
        aes_encryption_manager = AESEncryptionManager(encryption_key, iv)
        aes_encryption_manager.set_mode(self.encryption_type)

        while True:
            data = file.read(DATA_CHUNK)
            if not data:
                break

            encrypted_data = aes_encryption_manager.encrypt(data)

            peer.socket.send(encrypted_data)
            progress_bar.set(progress_bar.get() + step)
            self.peers[peer.username].upload_label.configure(text=f'{(progress_bar.get() * 100):.2f}%')

        self.peers[peer.username].upload_label.configure(text=f'Uploaded successfully!')
        file.close()
        progress_bar.destroy()
        elapsed_time = (time.perf_counter_ns() - start) / 1_000_000_000 / 60
        message_entry = MessageEntry(
            f'Upload finished. Elapsed time: {elapsed_time:.2f} min',
            'ME',
            'file-exchange',
            is_own=True
        )
        peer.messages_list.add_item(message_entry)

    def connect_to_user(self, user: BaseUser):
        result = tk.messagebox.askquestion(
            title="Establish connection",
            message=f"Are you sure you want to connect to user {user.username}"
        )

        if result == "yes":
            print(f'Connecting to user {user.username}')
            session_key = SessionKeyGenerator().generate_session_key()
            encrypted_session_key = RSAKeyManager.encrypt_with_public_key(user.public_key, session_key)

            peer = Peer(socket.socket(socket.AF_INET, socket.SOCK_STREAM), user.username, session_key)
            peer.socket.connect((user.url, int(user.port)))

            peer.socket.send(pickle.dumps({
                'type': 'init',
                'username': self.username,
                'session_key': encrypted_session_key
            }))
            self.peers[user.username] = peer
            create_widgets(peer, self)

        elif result == "no":
            return

    def refresh_users_list(self, users_list: list):
        for user in self.users:
            self.window.users_list.remove_item(user.username)
        self.users = list()

        for user in users_list:
            username, url, port, public_key = user.values()
            new_user = BaseUser(username, url, port, public_key)
            self.users.append(new_user)
            self.window.users_list.add_item(new_user)

    def refresh_messages_tab(self):
        username = self.window.messages_tabs.tab()
        peer: Peer = self.peers.get(username)
        peer.messages_list.clear()

        for message in peer.messages:
            peer.messages_list.add_item(message)

    def handle_messages(self, peer: Peer):
        while True and not self.stop_event.is_set():
            data: bytes = peer.socket.recv(DATA_CHUNK)
            if not data:
                break

            data: dict = pickle.loads(data)

            if 'init' in data.get('type'):
                peer.username = data.get('username')
                local_key = LocalKeyManager(key_directory=f'user-{self.username}').load_password_hash()
                private_key = RSAKeyManager(keys_directory=f'user-{self.username}').load_private_key(local_key)
                decrypted_session_key = RSAKeyManager.decrypt_with_private_key(private_key, data.get('session_key'))
                peer.session_key = decrypted_session_key

            elif 'message' in data.get('type'):
                print(f"Received encrypted message: {data.get('message')} from peer: {peer.username}")
                encryption_key, iv = peer.session_key[:16], peer.session_key[16:32]
                aes_encryption_manager = AESEncryptionManager(encryption_key, iv)
                message = aes_encryption_manager.decrypt(data.get('message')).decode('utf-8')
                message_entry = MessageEntry(message, peer.username, 'message-in', is_own=False)
                peer.messages.append(message_entry)
                self.peers[peer.username].messages_list.add_item(message_entry)
            elif 'file' in data.get('type'):
                start = time.perf_counter_ns()
                filename = data.get("name")
                file_size = data.get("size")
                message_entry = MessageEntry(
                    f'Receiving file "{filename}" of size {self.convert_size(file_size)}',
                    peer.username,
                    'file-exchange',
                    is_own=False
                )
                peer.messages.append(message_entry)
                self.peers[peer.username].messages_list.add_item(message_entry)
                encryption = data.get("encryption")

                step = 1 / (file_size / DATA_CHUNK)
                progress_bar = ctk.CTkProgressBar(
                    master=peer.progress_frame,
                    orientation=tk.HORIZONTAL,
                    width=300,
                    mode='determinate',
                    determinate_speed=step
                )
                progress_bar.set(0)
                progress_bar.pack(fill=ctk.BOTH)

                file_path = f"{self.user_directory}{filename}"
                encryption_key, iv = peer.session_key[:16], peer.session_key[16:32]
                aes_encryption_manager = AESEncryptionManager(encryption_key, iv)
                iv_len = len(iv)
                aes_encryption_manager.set_mode(encryption)
                with open(file_path, "wb") as file:
                    received_bytes = 0
                    while received_bytes < file_size:
                        file_data = peer.socket.recv(DATA_CHUNK + iv_len, socket.MSG_WAITALL)
                        data_decrypted = aes_encryption_manager.decrypt(file_data)
                        file.write(data_decrypted)
                        received_bytes += len(data_decrypted)

                        # Calculate and print the progress
                        progress = received_bytes / file_size * 100
                        progress_bar.set(progress)
                        self.peers[peer.username].upload_label.configure(text=f'{progress:.2f}%')

                    self.peers[peer.username].upload_label.configure(text=f'Received successfully!')
                    progress_bar.destroy()
                elapsed_time = (time.perf_counter_ns() - start) / 1_000_000_000 / 60
                message_entry = MessageEntry(
                    f'Received file. Elapsed time: {elapsed_time:.2f} min',
                    peer.username,
                    'file-exchange',
                    is_own=False
                )
                peer.messages.append(message_entry)
                self.peers[peer.username].messages_list.add_item(message_entry)

    @staticmethod
    def convert_size(size_bytes: int):
        if size_bytes == 0:
            return "0B"
        size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
        i = int(math.floor(math.log(size_bytes, 1024)))
        p = math.pow(1024, i)
        s = round(size_bytes / p, 2)
        return "%s %s" % (s, size_name[i])

    @staticmethod
    def send_message(peer: Peer):
        message: str = peer.entry.get()
        peer.entry.delete(0, ctk.END)

        if peer.socket is not None:
            encryption_key, iv = peer.session_key[:16], peer.session_key[16:32]
            aes_encryption_manager = AESEncryptionManager(encryption_key, iv)
            encrypted_message = aes_encryption_manager.encrypt(message.encode('utf-8'))
            print(f'Sending encrypted message: {encrypted_message}')

            peer.socket.send(pickle.dumps({
                'type': 'message',
                'message': encrypted_message
            }))
            message_entry = MessageEntry(message, 'ME', 'message-out', is_own=True)
            peer.messages_list.add_item(message_entry)


def create_widgets(peer: Peer, chat: ChatApp):
    try:
        tab = chat.window.messages_tabs.add(f'{peer.username}')
    except Exception:
        tab = chat.window.messages_tabs.tab(peer.username)

    messages_list = ScrollableMessagesList(master=tab, height=300)
    messages_list.pack(padx=10, pady=10, fill=ctk.BOTH)

    chat.peers[peer.username].set_messages_list(messages_list)

    input_frame = ctk.CTkFrame(master=tab)
    input_frame.pack(padx=10, pady=(10, 0), fill=ctk.BOTH)

    ctk.CTkLabel(
        master=input_frame,
        text="Enter message:",
        font=("Arial", 12),
    ).pack(padx=(10, 5), pady=10, side=ctk.LEFT)

    entry = ctk.CTkEntry(
        input_frame,
        font=("Arial", 12)
    )
    entry.pack(padx=(5, 10), pady=10, fill=ctk.BOTH)

    peer.set_entry(entry)

    buttons_frame = ctk.CTkFrame(master=tab, height=50)
    buttons_frame.pack(padx=10, pady=(10, 0), fill=ctk.BOTH)

    ctk.CTkButton(
        master=buttons_frame,
        text='Send message',
        command=lambda _peer=peer: chat.send_message(peer),
        fg_color=Color.PRIMARY,
        font=("Arial", 12)
    ).pack(padx=(10, 5), pady=10, side=ctk.LEFT)

    ctk.CTkButton(
        master=buttons_frame,
        text="Upload file",
        command=lambda _peer=peer: chat.upload_file(peer),
        fg_color=Color.PRIMARY,
        font=("Arial", 12)
    ).pack(padx=(5, 10), pady=10, side=ctk.RIGHT)

    progress_frame = ctk.CTkFrame(master=tab)
    progress_frame.pack(padx=10, pady=10, fill=ctk.BOTH)

    upload_label = ctk.CTkLabel(
        master=progress_frame,
        text="Upload status",
        fg_color=Color.PRIMARY,
        corner_radius=4,
        font=("Arial", 12),
    )
    upload_label.pack(padx=10, pady=10, fill=ctk.BOTH)

    chat.peers[peer.username].set_upload_label(upload_label)
    chat.peers[peer.username].set_progress_frame(progress_frame)


if __name__ == "__main__":
    app = ChatApp()
