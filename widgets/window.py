import customtkinter as ctk

import widgets
from const import (
    CONNECT,
    DISCONNECT,
)
from widgets.Color import Color


class App(ctk.CTk):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.geometry("900x650")
        self.title("SecurePeerExchange 1.0")
        self.resizable(width=False, height=False)

        self.configuration_frame = ctk.CTkFrame(self)
        self.configuration_frame.pack(padx=10, pady=(10, 0), fill=ctk.BOTH)

        self.user_info = ctk.CTkLabel(
            master=self.configuration_frame,
        )
        self.user_info.pack(padx=10, pady=10, side=ctk.LEFT)

        self.encryption_option = ctk.CTkOptionMenu(
            master=self.configuration_frame,
            values=["ECB", "CBC"],
            fg_color=Color.PRIMARY,
            button_color=Color.PRIMARY,
            dropdown_fg_color=Color.PRIMARY
        )
        self.encryption_option.set("Select Encryption")
        self.encryption_option.pack(padx=10, pady=10, side=ctk.RIGHT)

        self.connection_option = ctk.CTkOptionMenu(
            master=self.configuration_frame,
            values=[CONNECT, DISCONNECT],
            fg_color=Color.PRIMARY,
            button_color=Color.PRIMARY,
            dropdown_fg_color=Color.PRIMARY
        )
        self.connection_option.set("Server Connection")
        self.connection_option.pack(padx=10, pady=10, side=ctk.RIGHT)

        self.users_frame = ctk.CTkFrame(master=self)
        self.users_frame.pack(padx=(10, 5), pady=10, side=ctk.LEFT)

        self.messages_frame = ctk.CTkFrame(master=self)
        self.messages_frame.pack(padx=(5, 10), pady=10, fill=ctk.BOTH)

        self.connection_label = ctk.CTkLabel(
            master=self.users_frame,
            text="Not connected",
            fg_color=("red", "black"),
            font=("Arial", 12),
            corner_radius=20
        )
        self.connection_label.pack(padx=10, pady=(10, 0), fill=ctk.BOTH)

        self.users_label = ctk.CTkLabel(
            master=self.users_frame,
            text="Available users:",
            font=("Arial", 12),
            anchor='w'
        )
        self.users_label.pack(padx=10, pady=(10, 0), fill=ctk.BOTH)

        self.users_list = widgets.ScrollableUsersList(
            master=self.users_frame,
            height=500,
            width=300,
        )
        self.users_list.pack(padx=10, pady=10, fill=ctk.BOTH)

        self.messages_tabs = ctk.CTkTabview(
            master=self.messages_frame,
            height=50,
        )
        self.messages_tabs.pack(padx=10, pady=10, fill=ctk.BOTH)

    def update_user_info(self, text: str):
        self.user_info.configure(text=text)

    def set_messages_tab_command(self, func):
        self.messages_tabs.configure(command=func)

    def set_users_list_bind(self, func):
        self.users_list.bind('<Double-1>', func)
