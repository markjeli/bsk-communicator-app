import customtkinter as ctk

from crypto.LocalKeyManager import LocalKeyManager
from crypto.RSAKeyManager import RSAKeyManager
from validators.LoginValidator import LoginValidator
from validators.PasswordValidator import PasswordValidator
from widgets.Color import Color


class LoginWindow(ctk.CTk):
    def __init__(self):
        super().__init__()
        self.geometry("500x300")
        self.title("SecurePeerExchange 1.0")
        self.eval('tk::PlaceWindow . center')
        self.protocol("WM_DELETE_WINDOW", self._login_window_exit)
        self.resizable(width=False, height=False)

        self.font_size = 16
        self.error_message_size = 12
        self.padding_size = 32
        self.element_width = 300
        self.login_error_message = ctk.StringVar()
        self.password_error_message = ctk.StringVar()

        self.form_frame: ctk.CTkFrame = ctk.CTkFrame(master=self)
        self.form_frame.pack(anchor=ctk.CENTER, expand=True)

        self.label_login: ctk.CTkLabel = ctk.CTkLabel(
            master=self.form_frame,
            text="Login:",
            font=("Arial", self.font_size),
            anchor='w',
            width=self.element_width,
        )
        self.label_login.pack(padx=self.padding_size, pady=(self.padding_size, 0))

        self.entry_login = ctk.CTkEntry(
            master=self.form_frame,
            font=("Arial", self.font_size),
            width=self.element_width
        )
        self.entry_login.pack(padx=self.padding_size)

        self.label_login_error_message = ctk.CTkLabel(
            master=self.form_frame,
            textvariable=self.login_error_message,
            font=("Arial", self.error_message_size),
            text_color=Color.ERROR,
            anchor='w',
            width=self.element_width
        )
        self.label_login_error_message.pack()

        self.label_password = ctk.CTkLabel(
            master=self.form_frame,
            text="Password:",
            font=("Arial", self.font_size),
            anchor='w',
            width=self.element_width
        )
        self.label_password.pack(padx=self.padding_size)

        self.entry_password = ctk.CTkEntry(
            master=self.form_frame,
            show="*",
            font=("Arial", self.font_size),
            width=self.element_width
        )
        self.entry_password.pack(padx=self.padding_size)

        self.label_password_error_message = ctk.CTkLabel(
            master=self.form_frame,
            textvariable=self.password_error_message,
            font=("Arial", self.error_message_size),
            text_color=Color.ERROR,
            anchor='w',
            width=self.element_width
        )
        self.label_password_error_message.pack()

        self.button_login = ctk.CTkButton(
            master=self.form_frame,
            text="Login",
            command=self.login_user,
            fg_color=Color.PRIMARY,
            font=("Arial", self.font_size),
            width=self.element_width
        )
        self.button_login.pack(pady=(0, self.padding_size))

    def login_user(self):
        login = self.entry_login.get()
        password = self.entry_password.get()

        login_error, login_valid = LoginValidator.validate(login)
        password_error, password_valid = PasswordValidator.validate(password)

        self.login_error_message.set(login_error)
        self.password_error_message.set(password_error)

        if not login_valid or not password_valid:
            return

        keys_directory = f'user-{login}'

        local_key_manager = LocalKeyManager(keys_directory)

        if not local_key_manager.check_password_exists():
            local_key_manager.save_password_hash(password)

        if not local_key_manager.verify_password(password):
            self.password_error_message.set('Password is invalid')
            return

        rsa_key_manager = RSAKeyManager(key_length=2048, keys_directory=keys_directory)
        public_key, private_key = rsa_key_manager.generate_key_pair()
        local_key = local_key_manager.load_password_hash()
        rsa_key_manager.save_key_pair(public_key, private_key, local_key)

        self.quit()

    def _login_window_exit(self):
        self.destroy()
        raise SystemExit
