import os

import customtkinter as ctk
from PIL import Image

from models.MessageEntry import MessageEntry
from widgets.Color import Color


class Entry:
    def __init__(self):
        pass


class ScrollableMessagesList(ctk.CTkScrollableFrame):
    def __init__(self, master, command=None, **kwargs):
        super().__init__(master, **kwargs)
        self.grid(row=0, column=0, sticky="nsew")
        self.columnconfigure(0, weight=0)
        self.columnconfigure(1, weight=1)
        self.command = command
        self.labels_from = []
        self.labels_message = []

    def clear(self):
        for label_from, label_message in zip(self.labels_from, self.labels_message):
            label_from.destroy()
            label_message.destroy()
            self.labels_from.remove(label_from)
            self.labels_message.remove(label_message)

    def add_item(self, message_entry: MessageEntry):
        image = self._get_icon_by_type(message_entry.message_type)
        color = Color.PRIMARY if message_entry.is_own else Color.ERROR

        message_entry_frame = ctk.CTkFrame(master=self, fg_color='#000000', corner_radius=4)
        message_entry_frame.pack(padx=5, pady=(5, 0), fill=ctk.BOTH)

        label = ctk.CTkLabel(
            message_entry_frame,
            text=message_entry.message_from,
            image=image,
            compound='left',
            padx=5,
            anchor='w',
            text_color=color
        )
        label.grid(row=len(self.labels_from), column=0, pady=(0, 5), sticky='w')
        self.labels_from.append(label)

        msg = ctk.CTkLabel(
            message_entry_frame,
            text=message_entry.message,
            padx=5,
            anchor='w',
            text_color=color,
            justify='left'
        )
        msg.grid(row=len(self.labels_message), column=1, pady=(0, 5), sticky='we')
        self.update()
        msg.configure(wraplength=msg.winfo_width())
        self.labels_message.append(msg)

    @staticmethod
    def _get_icon_by_type(message_type: str):
        current_path = os.path.dirname(os.path.abspath(__file__))
        return ctk.CTkImage(Image.open(os.path.join(current_path, 'icons', f'{message_type}.png')))
