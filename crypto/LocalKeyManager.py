import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes


class LocalKeyManager:
    def __init__(self, key_directory: str):
        self.key_directory = key_directory
        self.password_hash_file = os.path.join(self.key_directory, '.key')

    def save_password_hash(self, local_password):
        if not self.check_password_exists():
            os.makedirs(self.key_directory)
        password_hash = self._hash_password(local_password)
        with open(self.password_hash_file, "w") as file:
            file.write(password_hash)

    @staticmethod
    def _hash_password(local_password):
        password_bytes = local_password.encode()
        digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        digest.update(password_bytes)
        password_hash = digest.finalize()
        return password_hash.hex()

    def verify_password(self, local_password):
        password_hash = self._hash_password(local_password)
        stored_password_hash = self.load_password_hash()
        return password_hash == stored_password_hash

    def load_password_hash(self):
        with open(self.password_hash_file, "r") as file:
            stored_password_hash = file.read().strip()
        return stored_password_hash

    def check_password_exists(self):
        return os.path.exists(self.password_hash_file)
