from cryptography.hazmat.primitives import padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


class AESEncryptionManager:
    def __init__(self, encryption_key, initialization_vector=None):
        self.key = encryption_key
        self.iv = initialization_vector
        self.mode: modes.Mode = modes.CBC(initialization_vector)

    def set_mode(self, aes_mode: str):
        if aes_mode == 'ECB':
            self.mode = modes.ECB()
        elif aes_mode == 'CBC':
            self.mode = modes.CBC(self.iv)
        else:
            raise ValueError("Invalid mode. Supported modes are 'ECB' and 'CBC'.")
        return self

    def encrypt(self, data_to_encrypt: bytes) -> bytes:
        cipher = Cipher(algorithms.AES(self.key), self.mode)
        encryptor = cipher.encryptor()
        padder = padding.PKCS7(128).padder()
        padded_data = padder.update(data_to_encrypt) + padder.finalize()
        return encryptor.update(padded_data) + encryptor.finalize()

    def decrypt(self, data_to_decrypt: bytes) -> bytes:
        cipher = Cipher(algorithms.AES(self.key), self.mode)
        decryptor = cipher.decryptor()
        unpadder = padding.PKCS7(128).unpadder()
        padded_data = decryptor.update(data_to_decrypt) + decryptor.finalize()
        return unpadder.update(padded_data) + unpadder.finalize()
