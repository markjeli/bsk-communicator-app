import os
from typing import Tuple

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding

from crypto.AESEncryptionManager import AESEncryptionManager


class RSAKeyManager:
    def __init__(self, key_length: int = 4096, keys_directory: str = "keys"):
        self.key_length: int = key_length
        self.keys_directory: str = keys_directory
        self.__priv_key_dir_name = ".private"
        self.public_key_path, self.private_key_path = self._get_key_paths()

    def generate_key_pair(self) -> Tuple[rsa.RSAPublicKey, rsa.RSAPrivateKey]:
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=self.key_length
        )
        public_key = private_key.public_key()
        return public_key, private_key

    def save_key_pair(self, public_key: rsa.RSAPublicKey, private_key: rsa.RSAPrivateKey, local_key) -> None:
        if not os.path.exists(self.keys_directory):
            os.makedirs(self.keys_directory)

        private_folder_path = os.path.join(self.keys_directory, self.__priv_key_dir_name)
        if not os.path.exists(private_folder_path):
            os.makedirs(private_folder_path)

        with open(self.public_key_path, "wb") as file:
            public_key_bytes = public_key.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )
            file.write(public_key_bytes)

        with open(self.private_key_path, "wb") as file:
            private_key_bytes = self._encrypt_private_key(private_key, local_key)
            file.write(private_key_bytes)

    def load_key_pair(self, local_key: str) -> Tuple[rsa.RSAPublicKey, rsa.RSAPrivateKey]:
        with open(self.public_key_path, "rb") as file:
            public_key = serialization.load_pem_public_key(
                file.read(),
                backend=default_backend()
            )

        with open(self.private_key_path, "rb") as file:
            private_key = self._decrypt_private_key(file.read(), local_key)

        return public_key, private_key

    def load_public_key(self) -> rsa.RSAPublicKey:
        with open(self.public_key_path, 'rb') as file:
            return serialization.load_pem_public_key(
                file.read(),
                backend=default_backend()
            )

    def load_private_key(self, local_key: str) -> rsa.RSAPrivateKey:
        with open(self.private_key_path, "rb") as file:
            return self._decrypt_private_key(file.read(), local_key)

    @staticmethod
    def get_bytes_from_public_key(public_key: rsa.RSAPublicKey) -> bytes:
        return public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )

    @staticmethod
    def get_public_key_from_bytes(public_key_bytes: bytes) -> rsa.RSAPublicKey:
        return serialization.load_pem_public_key(
            public_key_bytes,
            backend=default_backend()
        )

    @staticmethod
    def encrypt_with_public_key(public_key: rsa.RSAPublicKey, data_to_encrypt: bytes) -> bytes:
        return public_key.encrypt(
            data_to_encrypt,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )

    @staticmethod
    def decrypt_with_private_key(private_key: rsa.RSAPrivateKey, data_to_decrypt: bytes) -> bytes:
        return private_key.decrypt(
            data_to_decrypt,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )

    @staticmethod
    def _encrypt_private_key(private_key: rsa.RSAPrivateKey, local_key: str) -> bytes:
        iv, encryption_key = local_key[:16].encode(), local_key[16:32].encode()
        private_key_pem = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )
        aes_encryption_manager = AESEncryptionManager(encryption_key, iv)
        return aes_encryption_manager.encrypt(private_key_pem)

    @staticmethod
    def _decrypt_private_key(encrypted_private_key: bytes, local_key: str) -> rsa.RSAPrivateKey:
        iv, encryption_key = local_key[:16].encode(), local_key[16:32].encode()
        aes_encryption_manager = AESEncryptionManager(encryption_key, iv)
        decrypted_private_key = aes_encryption_manager.decrypt(encrypted_private_key)
        return serialization.load_pem_private_key(
            decrypted_private_key,
            password=None,
            backend=default_backend()
        )

    def _get_key_paths(self) -> Tuple[str, str]:
        public_key_path: str = os.path.join(self.keys_directory, f"public.pem")
        private_key_path: str = os.path.join(self.keys_directory, self.__priv_key_dir_name, "private.pem")

        return public_key_path, private_key_path
