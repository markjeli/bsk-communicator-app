import os

from cryptography.hazmat.primitives import hashes


class SessionKeyGenerator:
    @staticmethod
    def generate_session_key():
        session_key = os.urandom(32)
        session_key_hash = hashes.Hash(hashes.SHA256())
        session_key_hash.update(session_key)
        session_key_digest = session_key_hash.finalize()
        return session_key_digest
