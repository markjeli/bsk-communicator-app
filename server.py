import pickle
import socket
import threading
import time
from pprint import pprint
from typing import List

from crypto.RSAKeyManager import RSAKeyManager


class User:
    def __init__(self, client_socket: socket.socket, username, url, port, public_key: bytes):
        self.socket = client_socket
        self.username = username
        self.url = url
        self.port = port
        self.public_key = RSAKeyManager().get_public_key_from_bytes(public_key)


class Server:
    def __init__(self):
        self.host = "127.0.0.1"
        self.port = 12345
        self.users: List[User] = []

    def add_user(self, client_socket, username, url, port, public_key) -> User:
        user = User(client_socket, username, url, port, public_key)
        self.users.append(user)
        return user

    def remove_user(self, username):
        user = self.find_user(username)
        if user:
            self.users.remove(user)

    def find_user(self, username):
        for user in self.users:
            if user.username == username:
                return user
        return None

    def ping_clients(self):
        while True:
            for user in self.users:
                try:
                    user.socket.send(pickle.dumps({
                        'type': 'ping'
                    }))
                except Exception as e:
                    self.users.remove(user)
                    self.broadcast_users()
            time.sleep(.5)

    def broadcast_users(self):
        for user in self.users:
            users_list = [{
                'username': user.username,
                'url': user.url,
                'port': user.port,
                'public_key': RSAKeyManager.get_bytes_from_public_key(user.public_key)
            } for user in list(filter(lambda u: u.username != user.username, self.users))]
            user.socket.send(pickle.dumps({
                'type': 'users_list',
                'users': users_list
            }))


def handle_client_connection(client_socket: socket.socket, server: Server):
    data: bytes = client_socket.recv(1024)
    data: dict = pickle.loads(data)

    pprint(data)

    if 'login' in data.get('type'):
        _, username, url, port, public_key = data.values()
        print(f'Login: {username}, {url}, {port}')
        server.add_user(client_socket, username, url, port, public_key)
        server.broadcast_users()


def init_server(server: Server):
    try:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((server.host, server.port))
        server_socket.listen(1)
        return server_socket
    except Exception as e:
        print('Failed to start server! Retrying...')
        time.sleep(1)
        return init_server(server)


def run_server():
    server = Server()
    server_socket = init_server(server)
    print(f"Server listening on {server.host}:{server.port}...")
    threading.Thread(target=server.ping_clients).start()

    while True:
        client_socket, _ = server_socket.accept()
        threading.Thread(target=handle_client_connection, args=(client_socket, server,)).start()


run_server()
